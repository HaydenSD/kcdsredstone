var CONFIG = {
  "maps": {
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_isometric_cave": {
      "imageFormat": "png",
      "lastRendered": [
        1476447170,
        0,
        1476447170,
        0
      ],
      "maxZoom": 14,
      "name": "KCDS Redstone - Cave",
      "renderView": "isometric",
      "rotations": [
        0,
        2
      ],
      "textureSize": 12,
      "tileSetGroup": "world_isometric_t1",
      "tileSize": 384,
      "world": "world",
      "worldName": "world",
      "worldSeaLevel": 64
    },
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_isometric_day": {
      "imageFormat": "png",
      "lastRendered": [
        1476447170,
        0,
        1476447170,
        0
      ],
      "maxZoom": 14,
      "name": "KCDS Redstone - Day",
      "renderView": "isometric",
      "rotations": [
        0,
        2
      ],
      "textureSize": 12,
      "tileSetGroup": "world_isometric_t1",
      "tileSize": 384,
      "world": "world",
      "worldName": "world",
      "worldSeaLevel": 64
    },
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_isometric_night": {
      "imageFormat": "png",
      "lastRendered": [
        1476447170,
        0,
        1476447170,
        0
      ],
      "maxZoom": 14,
      "name": "KCDS Redstone - Night",
      "renderView": "isometric",
      "rotations": [
        0,
        2
      ],
      "textureSize": 12,
      "tileSetGroup": "world_isometric_t1",
      "tileSize": 384,
      "world": "world",
      "worldName": "world",
      "worldSeaLevel": 64
    },
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_topdown_day": {
      "imageFormat": "png",
      "lastRendered": [
        1476447170,
        0,
        1476447170,
        0
      ],
      "maxZoom": 13,
      "name": "KCDS Redstone - Topdown",
      "renderView": "topdown",
      "rotations": [
        0,
        2
      ],
      "textureSize": 6,
      "tileSetGroup": "world_topdown_t3",
      "tileSize": 288,
      "world": "world",
      "worldName": "world",
      "worldSeaLevel": 64
    }
  },
  "mapsOrder": [
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_isometric_day",
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_isometric_night",
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_isometric_cave",
    "https://gitlab.com/HaydenSD/kcdsredstone/raw/master/world_topdown_day"
  ],
  "tileSetGroups": {
    "world_isometric_t1": {
      "maxZoom": 14,
      "tileOffsets": [
        [
          0,
          0
        ],
        [
          0,
          0
        ],
        [
          0,
          0
        ],
        [
          0,
          0
        ]
      ],
      "tileWidth": 1
    },
    "world_topdown_t3": {
      "maxZoom": 13,
      "tileOffsets": [
        [
          0,
          0
        ],
        [
          0,
          0
        ],
        [
          0,
          0
        ],
        [
          0,
          0
        ]
      ],
      "tileWidth": 3
    }
  }
};
